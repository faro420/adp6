package adp6;

public class CustomSort {
    private int ccounter = 0;
    private int mcounter = 0;

    public int getC() {
        return ccounter;
    }

    public int getM() {
        return mcounter;
    }

    public void quicksort(int indexLeft, int indexRight, int min, int max,
            SortableObject[] array) {
        if ((indexRight - indexLeft) > 10) {
            int pivot;
            int i;
            int j;
            if (indexRight > indexLeft) {
                i = indexLeft;
                j = indexRight - 1;
                pivot = (min + max) / 2;
                while (true) {
                    while (array[i].getKey() < pivot) {
                        ccounter++;
                        i++;
                    }
                    while (array[j].getKey() >= pivot) {
                        ccounter++;
                        j--;
                        if (j <= 0) {
                            break;
                        }
                    }
                    if (i >= j) {
                        break;
                    }
                    swap(i, j, array);
                }
                if (i != indexRight) {
                    swap(i, indexRight, array);
                }
                quicksort(indexLeft, i - 1, min, pivot, array);
                quicksort(i + 1, indexRight, pivot, max, array);
            }
        } else {
            insertionSort(indexLeft, indexRight+1 , array);
        }
    }

	private void insertionSort(int l, int r, SortableObject[] array) {
		SortableObject valueToSort;
		int i, j;
		for (i = l; i < r; i++) {
			ccounter++;
			valueToSort = array[i];
			j = i;
			while (j > 0 && array[j - 1].getKey() > valueToSort.getKey()) {
				ccounter++;
				swap(j,j-1,array);
				j--;
			}
		}
	}

    private void swap(int i, int j, SortableObject[] array) {
        mcounter++;
        SortableObject temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
