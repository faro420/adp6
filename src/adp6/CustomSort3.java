package adp6;

public class CustomSort3 {
	private int ccounter = 0;
	private int mcounter = 0;

	public void sort(SortableObject[] array) {
		DoublePQuicksort(array, 0, array.length - 1);
	}

	private void DoublePQuicksort(SortableObject[] array, int left, int right) {
		int len = right - left;
		SortableObject puffer;
		if (len < 17) { // insertion sort on small array
			insertionSort(left, right + 1, array);
			return;
		}
		// median indexes
		int sixth = len / 6;
		int m1 = left + sixth;
		int m2 = m1 + sixth;
		int m3 = m2 + sixth;
		int m4 = m3 + sixth;
		int m5 = m4 + sixth;
		// 5-element sorting network
		if (array[m1].getKey() > array[m2].getKey()) {
			swap(m1, m2, array);
		}
		if (array[m4].getKey() > array[m5].getKey()) {
			swap(m4, m5, array);
		}
		if (array[m1].getKey() > array[m3].getKey()) {
			swap(m1, m3, array);
		}
		if (array[m2].getKey() > array[m3].getKey()) {
			swap(m2, m3, array);
		}
		if (array[m1].getKey() > array[m4].getKey()) {
			swap(m1, m4, array);
		}
		if (array[m3].getKey() > array[m4].getKey()) {
			swap(m3, m4, array);
		}
		if (array[m2].getKey() > array[m5].getKey()) {
			swap(m2, m5, array);
		}
		if (array[m2].getKey() > array[m3].getKey()) {
			swap(m2, m3, array);
		}
		if (array[m4].getKey() > array[m5].getKey()) {
			swap(m4, m5, array);
		}
		// pivots: [ < pivot1 | pivot1 <= && <= pivot2 | > pivot2 ]
		int pivot1 = array[m2].getKey();
		SortableObject pivot1data = array[m2];
		int pivot2 = array[m4].getKey();
		SortableObject pivot2data = array[m4];
		boolean diffPivots = pivot1 != pivot2;
		array[m2] = array[left];
		array[m4] = array[right];
		// center part pointers
		int less = left + 1;
		int great = right - 1;
		// sorting
		if (diffPivots) {
			for (int k = less; k <= great; k++) {
				ccounter++;
				puffer = array[k];
				if (puffer.getKey() < pivot1) {
					array[k] = array[less];
					array[less++] = puffer;
					mcounter++;
				} else if (puffer.getKey() > pivot2) {
					while (array[great].getKey() > pivot2 && k < great) {
						ccounter++;
						great--;
					}
					array[k] = array[great];
					array[great--] = puffer;
					puffer = array[k];
					mcounter++;
					if (puffer.getKey() < pivot1) {
						array[k] = array[less];
						array[less++] = puffer;
						mcounter++;
					}
				}
			}
		} else {
			for (int k = less; k <= great; k++) {
				ccounter++;
				puffer = array[k];
				if (puffer.getKey() == pivot1) {
					continue;
				}
				if (puffer.getKey() < pivot1) {
					array[k] = array[less];
					array[less++] = puffer;
					mcounter++;
				} else {
					while (array[great].getKey() > pivot2 && k < great) {
						ccounter++;
						great--;
					}
					array[k] = array[great];
					array[great--] = puffer;
					puffer = array[k];
					mcounter++;
					if (puffer.getKey() < pivot1) {
						array[k] = array[less];
						array[less++] = puffer;
						mcounter++;
					}
				}
			}
		}
		// swap
		array[left] = array[less - 1];
		array[less - 1] = pivot1data;
		mcounter++;
		array[right] = array[great + 1];
		array[great + 1] = pivot2data;
		mcounter++;
		// left and right parts
		DoublePQuicksort(array, left, less - 2);
		DoublePQuicksort(array, great + 2, right);
		// equal elements
		if (great - less > len - 13 && diffPivots) {
			for (int k = less; k <= great; k++) {
				ccounter++;
				puffer = array[k];
				if (puffer.getKey() == pivot1) {
					array[k] = array[less];
					array[less++] = puffer;
					mcounter++;
				} else if (puffer.getKey() == pivot2) {
					array[k] = array[great];
					array[great--] = puffer;
					puffer = array[k];
					mcounter++;
					if (puffer.getKey() == pivot1) {
						array[k] = array[less];
						array[less++] = puffer;
						mcounter++;
					}
				}
			}
		}
		// center part
		if (diffPivots) {
			DoublePQuicksort(array, less, great);
		}
	}

	private void insertionSort(int l, int r, SortableObject[] array) {
		SortableObject valueToSort;
		int i, j;
		for (i = l; i < r; i++) {
			ccounter++;
			valueToSort = array[i];
			j = i;
			while (j > 0 && array[j - 1].getKey() > valueToSort.getKey()) {
				ccounter++;
				swap(j,j-1,array);
				j--;
			}
		}
	}

	private void swap(int i, int j, SortableObject[] array) {
		mcounter++;
		SortableObject temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}

	public int getC() {
		return ccounter;
	}

	public int getM() {
		return mcounter;
	}
}
