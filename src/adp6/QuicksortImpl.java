package adp6;

import java.util.Random;

public class QuicksortImpl {
	private int ccounter = 0;
	private int mcounter = 0;

	public void quicksort(int indexLeft, int indexRight, SortableObject[] array) {
		int pivot;
		int i;
		int j;
		if (indexRight > indexLeft) {
			i = indexLeft;
			j = indexRight - 1;
			// pivot = array[indexRight].getKey();
			//pivot = pivotOfThree(indexLeft, indexRight, array);
			 pivot = pivotRandom(indexLeft, indexRight, array);
			while (true) {
				while (array[i].getKey() < pivot) {
					ccounter++;
					i++;
				}
				while (array[j].getKey() >= pivot) {
					ccounter++;
					j--;
					if (j <= 0) {
						break;
					}
				}
				if (i >= j) {
					break;
				}
				swap(i, j, array);
			}
			if (i != indexRight) {
				swap(i, indexRight, array);
			}
			quicksort(indexLeft, i - 1, array);
			quicksort(i + 1, indexRight, array);
		}
	}

	private void swap(int i, int j, SortableObject[] array) {
		mcounter++;
		SortableObject temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}

	private int pivotOfThree(int left, int right, SortableObject[] array) {
		int candidateleft = array[left].getKey();
		int candidateright = array[right].getKey();
		int middle = (left + right) / 2;
		int candidatemiddle = array[middle].getKey();
		int pivotcandidate = min(max(candidateleft, candidatemiddle),
				candidateright);
		if (array[middle].getKey() == pivotcandidate) {
			swap(middle, right, array);
		}
		if (array[left].getKey() == pivotcandidate) {
			swap(left, right, array);
		}
		return pivotcandidate;
	}

	private int pivotRandom(int left, int right, SortableObject[] array) {
		Random rand = new Random();
		int random = rand.nextInt((right - left) + 1) + left;
		swap(random, right, array);
		return array[right].getKey();

	}

	private int max(int i, int j) {
		if (i > j) {
			return i;
		}
			return j;
	}

	private int min(int i, int j) {
		if (i < j) {
			return i;
		}
		return j;
	}

	public void printArray(SortableObject[] array) {
		String ausgabe = "";
		for (int i = 0; i < array.length; i++) {
			ausgabe = ausgabe + array[i] + " ";
		}
		System.out.println(ausgabe);
	}

	public int getCCounter() {
		return ccounter;
	}

	public int getMCounter() {
		return mcounter;
	}

}
