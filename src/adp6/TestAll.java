package adp6;

import java.util.ArrayList;

public class TestAll {
    public static void main(String[] args) {
        int n = 1000000;// hier kannst du den Test steuern 10^n
        
        //KeyListgenerierung nach Vorgaben
        Keygen keygen = new Keygen(700*n, 800*n, n);
        ArrayList<Integer> list = keygen.getList();
        SortableObject[] array = new SortableObject[n];
        
        //Test von CustomSort
        System.out.println("CustomSort============================");
        for (int i = 0; i < n; i++) {
            array[i] = new SortableObject(list.get(i));
        }
        CustomSort sort = new CustomSort();
        sort.quicksort(0, n-1,700*n,800*n,array);
      //Die Sortierung wird ueberprueft
        for (int i = 0; i < n-1; i++) {
            if(array[i].getKey()>array[i+1].getKey()){
                System.out.println("CustomSort failed");
                break;
            }
        }
        System.out.println("C:"+sort.getC() + "\n"+"M:"+ sort.getM());
        System.out.println("QuickSort============================");
      //Test von QuickSort
        QuicksortImpl qsort = new QuicksortImpl(); 
        for (int i = 0; i < n; i++) {
            array[i] = new SortableObject(list.get(i));
        }
        qsort.quicksort(0, n-1,array);
      //Die Sortierung wird ueberprueft
        for (int i = 0; i < n-1; i++) {
            if(array[i].getKey()>array[i+1].getKey()){
                System.out.println("QuickSort failed");
                break;
            }
        }
        System.out.println("C:"+qsort.getCCounter() + "\n"+"M:"+ qsort.getMCounter());
        
      //Test von CustomSort2
        System.out.println("CustomSort2============================");
        CustomSort2 csort = new CustomSort2(); 
        for (int i = 0; i < n; i++) {
            array[i] = new SortableObject(list.get(i));
        }
        csort.quicksort(0, n-1,array);
      //Die Sortierung wird ueberprueft
        for (int i = 0; i < n-1; i++) {
            if(array[i].getKey()>array[i+1].getKey()){
                System.out.println("CustomSort2 failed");
                break;
            }
        }
        System.out.println("C:"+csort.getCCounter() + "\n"+"M:"+ csort.getMCounter());
      //Test von CustomSort2
        System.out.println("CustomSort3============================");
        CustomSort3 c3sort = new CustomSort3(); 
        for (int i = 0; i < n; i++) {
            array[i] = new SortableObject(list.get(i));
        }
        c3sort.sort(array);
      //Die Sortierung wird ueberprueft
        for (int i = 0; i < n-1; i++) {
            if(array[i].getKey()>array[i+1].getKey()){
                System.out.println("CustomSort3 failed");
                break;
            }
        }
        System.out.println("C:"+c3sort.getC() + "\n"+"M:"+ c3sort.getM());
    }
}
